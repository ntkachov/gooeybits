package com.thedailynerd.gooeybits;

import com.thedailynerd.gooeybits.phys.lattice.LatticePair;
import com.thedailynerd.gooeybits.phys.lattice.LatticePoint;
import com.thedailynerd.gooeybits.phys.utils.PhysUtils;

import junit.framework.TestCase;

import java.util.Random;

/**
 * Created by Nick on 7/18/15.
 */
public class PhysicsTest extends TestCase {

    public void testPairHashFunction(){
        Random random = new Random();
        LatticePoint a = new LatticePoint(random.nextFloat(), random.nextFloat(), 0);
        LatticePoint b = new LatticePoint(random.nextFloat() * 10, random.nextFloat(), 0);
        LatticePoint c = new LatticePoint(random.nextFloat(), random.nextFloat() * 10, 0);
        LatticePoint d = new LatticePoint(0,0,0);

        LatticePair pair1 = new LatticePair(a, b); //Should be same as 2
        LatticePair pair2 = new LatticePair(b, a); //Should be same as 1
        LatticePair pair3 = new LatticePair(a, c); //Should be unique
        LatticePair pair4 = new LatticePair(a, a); //should be unique
        LatticePair pair5 = new LatticePair(d, a);
        LatticePair pair6 = new LatticePair(d, d);


        assertEquals(pair1.hashCode(), pair2.hashCode());
        assertFalse(pair3.hashCode() == pair4.hashCode());
        assertFalse(pair1.hashCode() == pair4.hashCode());
        assertFalse(pair2.hashCode() == pair3.hashCode());
        assertFalse(pair5.hashCode() == pair6.hashCode());
        assertFalse(pair5.hashCode() == pair4.hashCode());
    }

    public void testAngleRotations(){
        LatticePoint a = new LatticePoint(1, 1, 0);
        LatticePoint b = new LatticePoint(1, 0, 0);
        LatticePoint c = new LatticePoint(b.getX() + 1, b.getY(), 0);

        double angle = a.getAngle(c);

//        float[] newPoints = PhysUtils.getCoordrdinatesForNewAngle(a, b, angle, a.getAngle(b), new float[2]);

//        LatticePoint newPoint = new LatticePoint(newPoints[0], newPoints[1], 0);
//        assertEquals(a.getAngle(c), a.getAngle(newPoint));
    }

    public void testShortestAngle(){
        double testAngle = Math.toRadians(45);
        for(double angle = testAngle; angle < testAngle + Math.PI ; angle += 0.001) {
            double shortestAngle = PhysUtils.getSignedShortestDistanceAngle(testAngle, angle);
            double target = angle - testAngle;
            String msg = ("testing < 180 : " +  Math.toDegrees(angle) + " : " + Math.toDegrees(shortestAngle) + "| expected " + target + " but had " + shortestAngle);
            assertTrue(msg, almostEqual(shortestAngle, target, 0.001));
        }

        for(double angle = testAngle + Math.PI; angle < testAngle + Math.PI * 2 ; angle += 0.1) {
            double shortestAngle = PhysUtils.getSignedShortestDistanceAngle(testAngle, angle);
            double target =  (Math.PI * 2d) - (angle - testAngle);
            String msg = ("testing > 180 : " +  Math.toDegrees(angle) + " : " + Math.toDegrees(shortestAngle) + "| expected " + target + " but had " + shortestAngle);
            assertTrue(msg, almostEqual(shortestAngle, target, 0.001));
          ;
        }
    }

    public static boolean almostEqual(double a, double b, double eps){
        return Math.abs(a-b)<eps;
    }
}
