package com.thedailynerd.gooeybits;

import android.graphics.drawable.Drawable;
import android.view.View;

import com.thedailynerd.gooeybits.phys.lattice.ops.LatticeOp;
import com.thedailynerd.gooeybits.phys.lattice.ops.LatticeOpBuilder;

/**
 * Created by Nick on 6/28/15.
 */
public class GooeyBits {

    private static int KEY_GOOEY_TAG = "gooey_bits".hashCode();

    /**
     * Will wrap the current drawable inside of a gooey container and make the view gooey.
     * Helper method around attaching a GooeyDrawable to a View.
     * @param goop The new view that will be turned into goop.
     */
    public static void makeGooey(View goop){
        GooeyDrawable gooeyDrawable = new GooeyDrawable(goop);
        goop.setBackground(gooeyDrawable);
        gooeyDrawable.resetCallback();
        goop.setTag(KEY_GOOEY_TAG, gooeyDrawable);
    }

    public static void interact(View gooey, Interaction interact) {
        interact(gooey, interact.mOp);
    }

    public static void interact(View gooey, LatticeOp latticeOp){
        GooeyDrawable drawable = getGooey(gooey);
        drawable.interact(latticeOp);
    }

    public static void step(View gooey) {
        GooeyDrawable drawable = getGooey(gooey);
        drawable.step();
    }

    public static GooeyDrawable getGooey(View gooey){
       return (GooeyDrawable) gooey.getTag(KEY_GOOEY_TAG);
    }

    public enum Interaction{
        BUMP(LatticeOpBuilder.newCenterOfMassOp(1, 1));

        private final LatticeOp mOp;

        Interaction(LatticeOp op) {
            mOp = op;
        }
    }

}
