package com.thedailynerd.gooeybits;

/**
 * Created by Nick on 10/16/2015.
 */
public class Constants {

    public final static float SANE_RIGIDITY = .2f;
    public final static float SANE_ELASTICITY = .4f;

}
