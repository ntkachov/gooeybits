package com.thedailynerd.gooeybits;

import android.annotation.TargetApi;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Outline;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.Region;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.media.MediaExtractor;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewOutlineProvider;
import android.view.ViewTreeObserver;
import android.widget.ImageView;

import com.thedailynerd.gooeybits.debug.DebugSettings;
import com.thedailynerd.gooeybits.debug.TestLattice;
import com.thedailynerd.gooeybits.debug.WireFrame;
import com.thedailynerd.gooeybits.drawable.Mesh;
import com.thedailynerd.gooeybits.phys.lattice.LatticeCreator;
import com.thedailynerd.gooeybits.phys.utils.simulation.Goop;
import com.thedailynerd.gooeybits.phys.lattice.ops.LatticeOp;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;

/**
 * Created by Nick on 6/28/15.
 */
public class GooeyDrawable extends Drawable implements ViewTreeObserver.OnPreDrawListener, Runnable, Drawable.Callback {

    private static final long DELAY = DebugSettings.TIME_STEP;
    private final static int MESH_SIZE = 5;

    private ViewProperties mViewProperties;
    private Paint mPaint = new Paint();
    private Goop mGooable;
    private Drawable mViewDrawable;
    private Bitmap mDrawableRenderPlane;
    private Canvas mRenderPlaneCanvas;
    private Mesh mMesh;
    View mView;

    public GooeyDrawable(View view){
        mView = view;
        mViewProperties = new ViewProperties(view);
        mGooable = new Goop(LatticeCreator.createMeshLattice(MESH_SIZE));
        mMesh = new Mesh(MESH_SIZE);
        mViewDrawable = view.getBackground();
        view.getViewTreeObserver().addOnPreDrawListener(this);
        startSettling();
    }

    public void resetCallback() {
        if(mViewDrawable != null) {
            mViewDrawable.setCallback(this);
        }
    }

    @Override
    public boolean onPreDraw() {
        mViewProperties.getNewViewProperties();
        if(mViewProperties.havePropertiesChanged()){
            invalidateSelf();
        }
        return true;
    }

    @Override
    public void draw(Canvas canvas) {
        mGooable.compute(mViewProperties);
        float centerX = canvas.getWidth() / 2;
        float centerY = canvas.getHeight() / 2;
        canvas.translate(centerX, centerY);

        if(mViewDrawable != null) {
            if(mDrawableRenderPlane == null){
                createDrawableRenderPlane(mViewDrawable);
            }

            mViewDrawable.draw(mRenderPlaneCanvas);
            mMesh.fromLattice(mGooable.getLattice(), canvas.getWidth(), canvas.getHeight());
//            canvas.drawBitmap(mDrawableRenderPlane, 0, 0, mPaint);
            canvas.drawBitmapMesh(mDrawableRenderPlane, MESH_SIZE - 1, MESH_SIZE - 1, mMesh.getVerts(), 0, null, 0, mPaint);
        }

        WireFrame.drawWireFrame(mGooable.getLattice(), canvas, mPaint);
    }

    public void interact(LatticeOp interaction) {
        mGooable.doAction(interaction);
        startSettling();
    }

    private void startSettling(){
        invalidateSelf();
        mView.removeCallbacks(this);
        mView.postDelayed(this, DELAY);
    }

    @Override
    public void run() {
//        if(!mGooable.isSettled()) {
            step();
            mView.postDelayed(this, DELAY);
//        } else {
//            Log.d("GooeyBits", "settled");
//        }
    }

    public void step() {
        mGooable.runFrame();
        invalidateSelf();
    }

    private void createDrawableRenderPlane(Drawable drawable){
        if(drawable == null){ return; }

        Rect bounds = drawable.getBounds();
        if(bounds != null){
            mDrawableRenderPlane = Bitmap.createBitmap(bounds.width(), bounds.height(), Bitmap.Config.ARGB_8888);
            mRenderPlaneCanvas = new Canvas(mDrawableRenderPlane);
        }
    }

    @Override
    public void setAlpha(int alpha) {
        if(mViewDrawable != null) {
            mViewDrawable.setAlpha(alpha);
        }
    }

    @Override
    public void setColorFilter(ColorFilter cf) {
        if(mViewDrawable != null) {
            mViewDrawable.setColorFilter(cf);
        }
    }

    @Override
    public int getOpacity() {
        if(mViewDrawable != null) {
            return mViewDrawable.getOpacity();
        } else {
            return 0;
        }
    }

    @Override
    public void setBounds(int left, int top, int right, int bottom) {
        super.setBounds(left, top, right, bottom);
        if(mViewDrawable != null) {
            mViewDrawable.setBounds(left, top, right, bottom);
        }
    }

    @Override
    public void setBounds(Rect bounds) {
        super.setBounds(bounds);
        if(mViewDrawable != null) {
            mViewDrawable.setBounds(bounds);
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    public Rect getDirtyBounds() {
        if(mViewDrawable != null) {
            return mViewDrawable.getDirtyBounds();
        }
        return super.getDirtyBounds();
    }

    @Override
    public void setChangingConfigurations(int configs) {
        super.setChangingConfigurations(configs);
        if(mViewDrawable != null) {
            mViewDrawable.setChangingConfigurations(configs);
        }
    }

    @Override
    public int getChangingConfigurations() {
        if(mViewDrawable != null) {
            return mViewDrawable.getChangingConfigurations();
        }
        return super.getChangingConfigurations();
    }

    @Override
    public void setDither(boolean dither) {
        super.setDither(dither);
        if(mViewDrawable != null) {
            mViewDrawable.setDither(dither);
        }
    }

    @Override
    public void setFilterBitmap(boolean filter) {
        super.setFilterBitmap(filter);
        if(mViewDrawable != null) {
            mViewDrawable.setFilterBitmap(filter);
        }
    }

//    @Override
//    public void invalidateSelf() {
//        super.invalidateSelf();
//        if(mViewDrawable != null) {
//            mViewDrawable.invalidateSelf();
//        }
//    }
//
//    @Override
//    public void scheduleSelf(Runnable what, long when) {
//        super.scheduleSelf(what, when);
//        if(mViewDrawable != null) {
//            mViewDrawable.scheduleSelf(what, when);
//        }
//    }
//
//    @Override
//    public void unscheduleSelf(Runnable what) {
//        super.unscheduleSelf(what);
//        if(mViewDrawable != null) {
//            mViewDrawable.unscheduleSelf(what);
//        }
//    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    @Override
    public int getAlpha() {
        if(mViewDrawable != null) {
            return mViewDrawable.getAlpha();
        }
        return super.getAlpha();
    }

    @Override
    public void setColorFilter(int color, PorterDuff.Mode mode) {
        super.setColorFilter(color, mode);
        if(mViewDrawable != null) {
            mViewDrawable.setColorFilter(color, mode);
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void setTint(int tint) {
        super.setTint(tint);
        if(mViewDrawable != null) {
            mViewDrawable.setTint(tint);
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void setTintList(ColorStateList tint) {
        super.setTintList(tint);
        if(mViewDrawable != null) {
            mViewDrawable.setTintList(tint);
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void setTintMode(PorterDuff.Mode tintMode) {
        super.setTintMode(tintMode);
        if(mViewDrawable != null) {
            mViewDrawable.setTintMode(tintMode);
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    public ColorFilter getColorFilter() {
        if(mViewDrawable != null) {
            return mViewDrawable.getColorFilter();
        }
        return super.getColorFilter();
    }

    @Override
    public void clearColorFilter() {
        super.clearColorFilter();
        if(mViewDrawable != null) {
            mViewDrawable.clearColorFilter();
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void setHotspot(float x, float y) {
        super.setHotspot(x, y);
        if(mViewDrawable != null) {
            mViewDrawable.setHotspot(x, y);
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void setHotspotBounds(int left, int top, int right, int bottom) {
        super.setHotspotBounds(left, top, right, bottom);
        if(mViewDrawable != null) {
            mViewDrawable.setHotspotBounds(left, top, right, bottom);
        }
    }

    @Override
    public boolean isStateful() {
        if(mViewDrawable != null) {
            return mViewDrawable.isStateful();
        }
        return super.isStateful();
    }

    @Override
    public boolean setState(int[] stateSet) {
        if(mViewDrawable != null) {
            return mViewDrawable.setState(stateSet);
        }
        return super.setState(stateSet);
    }

    @Override
    public int[] getState() {
        if(mViewDrawable != null) {
            return mViewDrawable.getState();
        }
        return super.getState();
    }

    @Override
    public void jumpToCurrentState() {
        super.jumpToCurrentState();
        if(mViewDrawable != null) {
            mViewDrawable.jumpToCurrentState();
        }
    }

    @Override
    public Drawable getCurrent() {
        if(mViewDrawable != null) {
            return mViewDrawable.getCurrent();
        }
        return super.getCurrent();
    }

    @Override
    public boolean setVisible(boolean visible, boolean restart) {
        if(mViewDrawable != null) {
            return mViewDrawable.setVisible(visible, restart);
        }
        return super.setVisible(visible, restart);
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    @Override
    public void setAutoMirrored(boolean mirrored) {
        super.setAutoMirrored(mirrored);
        if(mViewDrawable != null) {
            mViewDrawable.setAutoMirrored(mirrored);
        }
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    @Override
    public boolean isAutoMirrored() {
        if(mViewDrawable != null) {
            return mViewDrawable.isAutoMirrored();
        }
        return super.isAutoMirrored();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void applyTheme(Resources.Theme t) {
        super.applyTheme(t);
        if(mViewDrawable != null) {
            mViewDrawable.applyTheme(t);
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    public boolean canApplyTheme() {
        if(mViewDrawable != null) {
            return mViewDrawable.canApplyTheme();
        }
        return super.canApplyTheme();
    }

    @Override
    public Region getTransparentRegion() {
        if(mViewDrawable != null) {
            return mViewDrawable.getTransparentRegion();
        }
        return super.getTransparentRegion();
    }

    @Override
    public int getIntrinsicWidth() {
        if(mViewDrawable != null) {
            return mViewDrawable.getIntrinsicWidth();
        }
        return super.getIntrinsicWidth();
    }

    @Override
    public int getIntrinsicHeight() {
        if(mViewDrawable != null) {
            return mViewDrawable.getIntrinsicHeight();
        }
        return super.getIntrinsicHeight();
    }

    @Override
    public int getMinimumWidth() {
        if(mViewDrawable != null) {
            return mViewDrawable.getMinimumWidth();
        }
        return super.getMinimumWidth();
    }

    @Override
    public int getMinimumHeight() {
        if(mViewDrawable != null) {
            return mViewDrawable.getMinimumHeight();
        }
        return super.getMinimumHeight();
    }

    @Override
    public boolean getPadding(Rect padding) {
        if(mViewDrawable != null) {
            return mViewDrawable.getPadding(padding);
        }
        return super.getPadding(padding);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void getOutline(Outline outline) {
        super.getOutline(outline);
        if(mViewDrawable != null) {
            mViewDrawable.getOutline(outline);
        }
    }

    @Override
    public Drawable mutate() {
        if(mViewDrawable != null) {
            return mViewDrawable.mutate();
        }
        return super.mutate();
    }

    @Override
    public void inflate(Resources r, XmlPullParser parser, AttributeSet attrs) throws XmlPullParserException, IOException {
        super.inflate(r, parser, attrs);
        if(mViewDrawable != null) {
            mViewDrawable.inflate(r, parser, attrs);
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void inflate(Resources r, XmlPullParser parser, AttributeSet attrs, Resources.Theme theme) throws XmlPullParserException, IOException {
        super.inflate(r, parser, attrs, theme);
        if(mViewDrawable != null) {
            mViewDrawable.inflate(r, parser, attrs, theme);
        }
    }

    @Override
    public ConstantState getConstantState() {
        if(mViewDrawable != null) {
            return super.getConstantState();
        }
        return super.getConstantState();
    }

    @Override
    public void invalidateDrawable(Drawable who) {
        invalidateSelf();
    }

    @Override
    public void scheduleDrawable(Drawable who, Runnable what, long when) {
        scheduleSelf(what, when);
    }

    @Override
    public void unscheduleDrawable(Drawable who, Runnable what) {
        unscheduleSelf(what);
    }

}
