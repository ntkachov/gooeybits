package com.thedailynerd.gooeybits.debug;

import com.thedailynerd.gooeybits.phys.utils.simulation.FrameCalculator;

/**
 * Created by Nick on 9/27/2015.
 */
public class DebugFrameCalculator extends FrameCalculator {

    @Override
    public double getFrameStep() {
        return 1;
    }
}
