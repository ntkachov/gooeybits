package com.thedailynerd.gooeybits.debug;

import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.CornerPathEffect;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;

import com.thedailynerd.gooeybits.drawable.ConvexHullDrawableConverter;
import com.thedailynerd.gooeybits.phys.utils.PathUtils;
import com.thedailynerd.gooeybits.phys.utils.PhysUtils;
import com.thedailynerd.gooeybits.phys.lattice.Lattice;
import com.thedailynerd.gooeybits.phys.lattice.LatticePoint;

import org.apache.http.impl.conn.Wire;

/**
 * Created by Nick on 7/18/15.
 */
public class WireFrame {

    public static Bitmap bitmapForMesh;
    public static int meshSize = (((DebugSettings.MESH_SIZE ) * (DebugSettings.MESH_SIZE)) * 2);
    public static float[] verts = new float[meshSize];

    /**
     * A debug method used to draw the points of the lattice
     * @param lattice the lattice to draw
     * @param canvas the canvas to draw to
     * @param paint the paint that will be used to draw. for best results, call adjustPaint on
     *              the paint object beforehand and resetPaint after calling this method
     */
    public static void drawWireFrameDots(Lattice lattice, Canvas canvas, Paint paint, float scale){
        int paintColor = paint.getColor();
        for(int pointIter = 0; pointIter < lattice.getPoints().size(); pointIter++){
            LatticePoint point = lattice.getPoints().get(pointIter);
            if(point == TestLattice.dragPoint){
                paint.setColor(Color.RED);
            }
            float x = getPointX(point, scale, lattice);
            float y = getPointY(point, scale, lattice);
            canvas.drawPoint(x, y, paint);
            paint.setColor(paintColor);
            if(DebugSettings.DRAW_POINTS_NUMBERS) {
                canvas.drawText(pointIter + "", x - 5, y - 20, paint);
            }
        }
    }

    /**
     * A debug method to draw the wire frame of the latice to a canvas.
     * @param lattice the lattice to draw
     * @param canvas the canvas to draw to
     * @param paint the paint that will be used in canvas.drawLine
     */
    public static void drawWireFrameLines(Lattice lattice, Canvas canvas, Paint paint, float scale){
        for(int pointIter = 0; pointIter < lattice.getPoints().size(); pointIter++){
            LatticePoint point = lattice.getPoints().get(pointIter);
            for (int connIter = 0; connIter < point.getConnections().size(); connIter++){
                LatticePoint.LatticeConnection connection = point.getConnections().get(connIter);
                canvas.drawLine(getPointX(point, scale, lattice), getPointY(point, scale, lattice),
                        getPointX(connection.getConnectedPoint(), scale, lattice), getPointY(connection.getConnectedPoint(), scale, lattice), paint);
            }
        }
    }

    public static void drawPath(Lattice lattice, Canvas canvas, Paint paint, float scale){
        Path path = PathUtils.toSimplePath(lattice);
        Matrix matrix = new Matrix();
        matrix.setScale(scale, scale);
        if(DebugSettings.FOCUS_COM) {
            matrix.preTranslate(-lattice.getCenterOfMassX(), -lattice.getCenterOfMassY());
        }
        path.transform(matrix);
//        paint.setPathEffect(new CornerPathEffect(30));
        paint.setColor(Color.BLUE);
        canvas.drawPath(path, paint);
    }

    private static float getPointX(LatticePoint point, float scale, Lattice lattice){
        return getX(point.getX(), scale, lattice);
    }

    private static float getX(double x, float scale, Lattice lattice){
        if(DebugSettings.FOCUS_COM) {
            return getCoord(x, lattice.getCenterOfMassX(), scale);
        } else {
            return getCoord(x, 0, scale);
        }
    }

    private static float getPointY(LatticePoint point, float scale, Lattice lattice){
        return getY(point.getY(), scale, lattice);
    }

    private static float getY(double y, float scale, Lattice lattice){
        if(DebugSettings.FOCUS_COM) {
            return getCoord(y, lattice.getCenterOfMassY(), scale);
        } else {
            return getCoord(y,0, scale);
        }
    }

    private static float getCoord(double val, double centerOfMass, float scale){
        return (float) ((val - centerOfMass)) * scale;
    }

    /**
     * Default stroke adjustment
     */
    private static final float STROKE_ADJUST = 10;

    /**
     * Adjusts the paint to make wireframe dots more visible
     * @param paint paint to adjust
     * @return the adjusted paint
     */
    public static Paint adjustPaint(Paint paint){
        internalAdjustPaint(paint, STROKE_ADJUST);
        return paint;
    }

    /**
     * resets the paint after the paint was adjusted with adjustPaint
     * @param paint the paint that was returned to normal
     * @return the origin paint
     */
    public static Paint resetPaint(Paint paint){
        internalAdjustPaint(paint, -STROKE_ADJUST);
        return paint;
    }

    /**
     * Adjusts paint stroke by a given amount
     * @param paint paint to adjust
     * @param adjust amount to change the stroke with by
     */
    private static void internalAdjustPaint(Paint paint, float adjust){
        float stroke = paint.getStrokeWidth();
        stroke += adjust;
        paint.setStrokeWidth(stroke);
    }

    public static void drawDesired(Lattice lattice, Canvas canvas, Paint paint, float scale) {
        for(int pointIter = 0; pointIter < lattice.getPoints().size(); pointIter++){
            LatticePoint point = lattice.getPoints().get(pointIter);
            for (int connIter = 0; connIter < point.getConnections().size(); connIter++){
                LatticePoint.LatticeConnection connection = point.getConnections().get(connIter);
                  drawNextLine(point, connection, scale, lattice, canvas, paint);
                if(DebugSettings.DRAW_ROTATION) {
                    int color = paint.getColor();
                    paint.setColor(Color.DKGRAY);
                    WireFrame.drawAngles(point, connection, scale, lattice, pointIter, connIter, canvas, paint);
                    paint.setColor(color);
                }
            }
        }
    }

    public static void drawNextLine(LatticePoint point, LatticePoint.LatticeConnection connection, float scale, Lattice lattice, Canvas canvas, Paint paint){
//        float distance = .calculateDistanceForPoints(lattice.getElasticity(), DebugSettings.TIME_STEP, connection);
//        double rotation = AngleUtils.calculateNewAngleForPoints(lattice.getElasticity(), DebugSettings.TIME_STEP, connection);
//        LatticePoint dom = connection.getDominant(), sub = connection.getConnectedPoint();
//        float[] newpoint = PhysUtils.getCoordinatesForDistanceAndAngle(dom, sub, rotation, distance, new float[2] );
//
//        float x = getPointX(point, scale, lattice);
//        float y =  getPointY(point, scale, lattice);
//
//        float x1 = getX(newpoint[0], scale, lattice);
//        float y1 = getY(newpoint[1], scale, lattice);
//        canvas.drawLine(x, y, x1, y1, paint);
    }

    public static void drawAngles(LatticePoint point, LatticePoint.LatticeConnection connection, float scale, Lattice lattice,int pointnum, int con, Canvas canvas, Paint paint){
        float x = getPointX(point, scale, lattice);
        float y =  getPointY(point, scale, lattice);

        int cur = (int) Math.round(Math.toDegrees(connection.getCurrentAngle()));
        int des = (int) Math.round(Math.toDegrees(connection.getDesiredAngle()));
        canvas.drawText(pointnum + ": " + cur + "/" +des, x + 15, y + (25 * con), paint);
    }

   public static void drawWireFrame(Lattice lattice, Canvas canvas, Paint paint){
        paint.setStrokeCap(Paint.Cap.ROUND);
        float scale = DebugSettings.SCALE;

       if (DebugSettings.DRAW_BITMAP_MESH) {
           if(bitmapForMesh != null){
               float[] verts = getVerts(lattice);
               canvas.scale(scale, scale);
               canvas.drawBitmapMesh(bitmapForMesh, DebugSettings.MESH_SIZE - 1, DebugSettings.MESH_SIZE - 1, verts, 0, null, 0, paint);
               canvas.scale(1/scale, 1/scale);
           }
       }

        if(DebugSettings.DRAW_WIREFRAMES) {
            paint.setStrokeWidth(2);
            WireFrame.drawWireFrameLines(lattice, canvas, paint, scale);
            WireFrame.adjustPaint(paint);
            WireFrame.drawWireFrameDots(lattice, canvas, paint, scale);
            WireFrame.resetPaint(paint);
//            canvas.drawPath(Lattice);
        }
        if(DebugSettings.DRAW_ANGLES) {
            int color = paint.getColor();
            paint.setColor(Color.MAGENTA);
            WireFrame.drawDesired(lattice, canvas, paint, scale);
            paint.setColor(color);
        }

       if(DebugSettings.DRAW_PATH){
           paint.setStyle(Paint.Style.FILL);
           WireFrame.drawPath(lattice, canvas, paint, scale);
       }

       if(DebugSettings.DRAW_COM){
           int color = paint.getColor();
           paint.setColor(Color.YELLOW);
           float x = getX(lattice.getCenterOfMassX(), scale, lattice);
           float y = getY(lattice.getCenterOfMassY(), scale, lattice);
           canvas.drawCircle(x, y, 5, paint);
           paint.setColor(color);
       }



//       if(ConvexHullDrawableConverter.testresults != null) {
//           paint.setColor(Color.BLUE);
//           WireFrame.drawPath(ConvexHullDrawableConverter.testresults, canvas, paint, 3);
//       }
    }

    private static float[] getVerts(Lattice lattice) {
        int vertsIter = 0;
        for(int point = 0; point < lattice.size(); point++){
            verts[vertsIter++] = lattice.getCenterAdjustedX(point);
            verts[vertsIter++] = lattice.getCenterAdjustedY(point);
        }

        return verts;
    }

}
