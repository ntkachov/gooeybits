package com.thedailynerd.gooeybits.debug;

/**
 * Created by Nick on 8/2/2015.
 */
public class DebugSettings {

    public static final float DEFAULT_RADIUS = .5f;
    public static float SCALE = 75;
    public static boolean
            FOCUS_COM = true,
            DRAW_COM = true,
            DRAW_WIREFRAMES = false,
            DRAW_POINTS_NUMBERS = false, //Causes more memory usage & GCs
            DRAW_ANGLES = false,
            DRAW_ROTATION = false,
            DRAW_BITMAP_MESH = false,
            DRAW_PATH = false;

    public static int MESH_SIZE = 5;


    public static long TIME_STEP = 16;
    public static long SIM_SPEED = 16;

}
