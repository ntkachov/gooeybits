package com.thedailynerd.gooeybits.debug;

import com.thedailynerd.gooeybits.drawable.ConvexHullDrawableConverter;
import com.thedailynerd.gooeybits.phys.lattice.Lattice;
import com.thedailynerd.gooeybits.phys.lattice.LatticePoint;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nick on 7/18/15.
 */
public class TestLattice {

    public static int testPoint = 0;
    public static LatticePoint dragPoint;
    public static Lattice testLattice;
    public static List<LatticePoint.LatticeConnection> changeConnection = new ArrayList<>();

    public static Lattice getTestLattice(){
//        return ConvexHullDrawableConverter.testresults;
//        return getTestSquareLattice();
//        return getCircleTestLattice();
        return getTestMeshLattice();
    }

    private static Lattice getTwoPointLattice() {
        testLattice = new Lattice();
        testLattice.setRigidity(0.2f);
        testLattice.setElasticity(0.5f);

        LatticePoint a = new LatticePoint(0, 0, 4);
        LatticePoint b = new LatticePoint(-1, 1, 3);
        a.lock();
        a.addConnection(b);
        b.addConnection(a);

        testLattice.addAll(a);
        dragPoint = b;
        return testLattice;
    }

    public static Lattice getCircleTestLattice(){
        testLattice = new Lattice();
        testLattice.setRigidity(.5f);
        testLattice.setElasticity(0.9f);

        LatticePoint prev = null, first = null;
//        LatticePoint center = new LatticePoint(0,0, 4);
//        center.setWeight(2);
        int mult = 5;
        for (int i = 0; i < 360; i+= 20){
            double rads = Math.toRadians(i);
            LatticePoint point = new LatticePoint((float)Math.cos(rads) * mult, (float)Math.sin(rads) * mult, 3);
//            point.addConnection(center);
//            center.addConnection(point);
            if(prev != null){
                point.addConnection(prev);
                prev.addConnection(point);
            } else {
                first = point;
            }
            prev = point;
        }

        prev.addConnection(first);
        first.addConnection(prev);
        testLattice.addAll(prev);
        dragPoint = testLattice.getPoint(testPoint);
        return testLattice;
    }

    public static Lattice getTestMeshLattice(){
        int size = DebugSettings.MESH_SIZE;
        LatticePoint[][] latticePoints = new LatticePoint[size][size];
        testLattice = new Lattice();
        for(int x = 0; x < size; x++){
            for(int y = 0; y < size; y++){
                LatticePoint point = new LatticePoint(x/(float)size,y/(float)size,2);
                latticePoints[x][y] = point;
                if(x > 0){
                    LatticePoint conn = latticePoints[x-1][y];
                    conn.addConnection(point);
                    point.addConnection(conn);
                }
                if(y > 0){
                    LatticePoint conn = latticePoints[x][y-1];
                    conn.addConnection(point);
                    point.addConnection(conn);
                }
                testLattice.addPoint(point);
            }
        }
        dragPoint = testLattice.getPoint(testPoint);
        testLattice.setRigidity(0.2f);
        testLattice.setElasticity(0.3f);

        int x = latticePoints.length -1;
        for(int y = 0; y < latticePoints[x].length; y++){
            changeConnection.add(latticePoints[x][y].findConnectionTo(latticePoints[x-1][y]));
            changeConnection.add(latticePoints[x-1][y].findConnectionTo(latticePoints[x][y]));
        }

        return testLattice;

    }

    public static Lattice getTestSquareLattice(){
        testLattice = new Lattice();
        LatticePoint pointTopRight = new LatticePoint(-1, -1, 2);
        LatticePoint pointMidRight = new LatticePoint(-1, 0, 3);
        LatticePoint pointBotRight = new LatticePoint(-1, 1, 2);
        
        LatticePoint pointTopMid = new LatticePoint(0, -1, 3);
//        LatticePoint  = new LatticePoint(0, 0, 4);
        LatticePoint pointBotMid = new LatticePoint(0, 1, 3);

        LatticePoint pointTopLeft = new LatticePoint(1, -1, 2);
        LatticePoint pointMidLeft = new LatticePoint(1, 0, 3);
        LatticePoint pointBotLeft = new LatticePoint(1, 1, 2);
        
        pointTopRight.addConnection(pointTopMid, pointMidRight);
        pointMidRight.addConnection(pointTopRight,  pointBotRight);
        pointBotRight.addConnection(pointMidRight, pointBotMid);
//
        pointTopMid.addConnection(pointTopRight, pointTopLeft );
//        .addConnection(pointTopMid, pointMidLeft, pointMidRight);//, pointBotMid, pointTopRight, pointTopLeft, pointBotRight, pointBotLeft);
        pointBotMid.addConnection(pointBotRight, pointBotLeft );

        pointTopLeft.addConnection(pointTopMid, pointMidLeft);
        pointMidLeft.addConnection(pointTopLeft, pointBotLeft);
        pointBotLeft.addConnection(pointBotMid, pointMidLeft);

        dragPoint = pointTopMid;
        testLattice.addAll(dragPoint);
        testLattice.setRigidity(0.2f);
        testLattice.setElasticity(0.05f);

        changeConnection.add(pointTopLeft.getConnections().get(0));
        changeConnection.add(pointTopMid.getConnections().get(1));
        changeConnection.add(pointBotLeft.getConnections().get(0));
        changeConnection.add(pointBotMid.getConnections().get(1));

        return testLattice;
    }

    public static Lattice getLineTestLattice(){
        testLattice = new Lattice();
        LatticePoint point = new LatticePoint(-1, 0, 2);
        LatticePoint prev = point;
        LatticePoint addPoint = null;
        int points = 9;
        for(int i = 1; i < points; i++){
            float x = 2f * ((float)i / (float)points);
            addPoint = new LatticePoint(-1 + x, 0, 2);
            prev.addConnection(addPoint);
            addPoint.addConnection(prev);
            prev = addPoint;
        }

        testLattice.addAll(point);
        testLattice.setRigidity(0.8f);
        testLattice.setElasticity(0.2f);
        dragPoint = point;
        point.lock();
        addPoint.lock();
        return testLattice;

    }

    public static void next(){
        testPoint = (testPoint + 1) % testLattice.getPoints().size();
//        dragPoint.setWeight(1);
        dragPoint = testLattice.getPoints().get(testPoint);
//        dragPoint.setWeight(2);
    }

}
