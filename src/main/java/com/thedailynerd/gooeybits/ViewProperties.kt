package com.thedailynerd.gooeybits

import android.annotation.SuppressLint
import android.support.v7.internal.VersionUtils
import android.view.View

import java.lang.ref.WeakReference

/**
 * Created by Nick on 7/4/15.

 */
class ViewProperties(view: View) {

    enum class Property {
        ROTATION, ROTATIONX, ROTATIONY, TRANSLATIONX,
        TRANSLATIONY, TRANSLATIONZ, HEIGHT, WIDTH;

        @SuppressLint("NewApi") //We use protection against new APIs
        internal fun getProperty(view: View): Float {
            when (this) {
                ROTATION -> return view.rotation
                ROTATIONX -> return view.rotationX
                ROTATIONY -> return view.rotationY
                TRANSLATIONX -> return view.translationX
                TRANSLATIONY -> return view.translationY
                TRANSLATIONZ -> {
                    if (VersionUtils.isAtLeastL()) {
                        return view.translationZ
                    } else {
                        return 0f
                    }
                    return view.layoutParams.height.toFloat()
                }
                HEIGHT -> return view.layoutParams.height.toFloat()
                WIDTH -> return view.layoutParams.width.toFloat()
                else -> return 0f
            }
        }
    }

    private val mView: WeakReference<View>
    private val mRecordedViewProperties: FloatArray
    private val previousViewProperties: FloatArray
    private val mViewPropertiesDelta: FloatArray
    private val mPropertiesArray = Property.values()

    init {
        mView = WeakReference(view)
        mRecordedViewProperties = FloatArray(PROPERTIES_COUNT)
        mViewPropertiesDelta = FloatArray(PROPERTIES_COUNT)
        previousViewProperties = FloatArray(PROPERTIES_COUNT)
    }

    private fun recordCurrentViewProperties() {
        val view = mView.get()

        for (property in 0..PROPERTIES_COUNT - 1) {
            val prop = mPropertiesArray[property]
            mRecordedViewProperties[prop.ordinal] = prop.getProperty(view)
        }
    }

    private fun createDeltaViewProperties() {
        for (property in 0..PROPERTIES_COUNT - 1) {
            mViewPropertiesDelta[property] = mRecordedViewProperties[property] - previousViewProperties[property]
        }
    }

    /**
     * Determines if any of the view proerties have been changed.
     * @return true if the view is different since the last time we checked. false otherwise.
     */
    fun havePropertiesChanged(): Boolean {
        for (property in 0..PROPERTIES_COUNT - 1) {
            val propChange = Math.signum(mViewPropertiesDelta[property])
            if (propChange != 0f) {
                return true
            }
        }
        return false
    }

    /**
     * Generates the new view properties for this frame
     */
    fun getNewViewProperties() {
        //Copy current values into the previous slot;
        System.arraycopy(mRecordedViewProperties, 0, previousViewProperties, 0, mRecordedViewProperties.size)
        recordCurrentViewProperties()
        createDeltaViewProperties()
    }

    fun getCurrentViewProperty(property: Property): Float {
        return mRecordedViewProperties[property.ordinal]
    }

    fun getDeltaViewProperty(property: Property): Float {
        return mViewPropertiesDelta[property.ordinal]
    }

    companion object {

        private val PROPERTIES_COUNT = Property.values().size
    }
}
