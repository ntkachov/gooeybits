package com.thedailynerd.gooeybits

import android.view.View
import com.thedailynerd.gooeybits.debug.TestLattice
import com.thedailynerd.gooeybits.phys.lattice.LatticePoint
import com.thedailynerd.gooeybits.phys.utils.HitUtils
import com.thedailynerd.gooeybits.phys.utils.PhysUtils
import com.thedailynerd.gooeybits.phys.utils.simulation.Goop

/**
 * Created by Nick on 2/6/2016.
 */

class ViewPropertiesApplicator(val goop: Goop){

    fun apply(viewProperties: ViewProperties){
        translation(viewProperties);
    }

    fun translation(viewProperties: ViewProperties){
        val x = viewProperties.getDeltaViewProperty(ViewProperties.Property.TRANSLATIONX)
        val y = viewProperties.getDeltaViewProperty(ViewProperties.Property.TRANSLATIONY)
        //we currently don't handle Z

        val point = HitUtils.getDragPointOnVector(goop.lattice, x, y);
        TestLattice.dragPoint = point
        goop.lattice.movePoint(point, x * 0.01f , y * 0.01f)
    }

}