package com.thedailynerd.gooeybits.phys;

/**
 * Created by Nick on 7/8/15.
 */
public class Line {

    private Line mNext, mPrevious;
    float mX, mY;

    public Line(float x, float y) {
        mX = x;
        mY = y;
    }

    public Line next() {
        return mNext;
    }

    public void setNext(Line next) {
        mNext = next;
    }

    public Line previous() {
        return mPrevious;
    }

    public void setPrevious(Line previous) {
        mPrevious = previous;
    }
}