package com.thedailynerd.gooeybits.phys.lattice.ops;

import com.thedailynerd.gooeybits.phys.lattice.Lattice;

/**
 * Created by Nick on 7/18/15.
 */
public class LatticeOperator {

    private final Lattice mLattice;

    public LatticeOperator(Lattice lattice) {
        mLattice = lattice;
    }

    public void resolve(LatticeOp interaction) {
        interaction.operate(mLattice);
    }


}


