package com.thedailynerd.gooeybits.phys.lattice;

import android.graphics.Path;
import android.graphics.RectF;

import com.thedailynerd.gooeybits.phys.utils.PhysUtils;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * A Lattice is a representation of a set of points. Each point can have connections to other points
 * and the properties of the lattice determine how these connections play out.
 * A lattice does not need to have all points connected to one another but disjointed sections of the
 * lattice will behave as seperate lattices.
 */
public class Lattice {

    private ArrayList<LatticePoint> mLatticePoints;
    private ArrayList<LatticePoint> mOutlinePoint;
    private float mMassCenterX, mMassCenterY;
    private float mRunningTotalPointsX, mRunningTotalPointsY;
    private float mRigidity, mElasticity;
    private Boolean mIsMesh;
    private Path mOutlinePath;
    private LatticePoint mCenterMostPoint;
    private float mRadius; //the radius of the smallest circle that encompases the entire lattice.

    /**
     * initializes a new empty lattice
     */
    public Lattice(){
        mLatticePoints = new ArrayList<>();
        mOutlinePoint = new ArrayList<>();
        mOutlinePath = new Path();
    }

    /**
     * Uses a simple iterative breadth first search to add all of the connected lattice points into the lattice;
     * @param startingPoint any point in a connected lattice;
     */
    public void addAll(LatticePoint startingPoint) {
        LinkedList<LatticePoint> scanQueue = new LinkedList<>();
        scanQueue.add(startingPoint);

        while(!scanQueue.isEmpty()){

            LatticePoint pop = scanQueue.pop();
            if(!mLatticePoints.contains(pop)) {
                addPoint(pop);

                for (LatticePoint.LatticeConnection connection : pop.getConnections()) {
                    LatticePoint connected = connection.mConnectedTo;
                    scanQueue.add(connected);
                }
            }
        }
    }

    /**
     * Adds a point to the current lattice.
     * note: This does not connect the point of the lattice but simply adds it to the calculations;
     * @param point
     */
    public void addPoint(LatticePoint point){
        mLatticePoints.add(point);
        addPointToMetadata(point);
    }

    private void addPointToMetadata(LatticePoint point){
        mRunningTotalPointsX += point.getX();
        mRunningTotalPointsY += point.getY();
        mMassCenterX = mRunningTotalPointsX / (float)size();
        mMassCenterY = mRunningTotalPointsY / (float)size();
        float pointDistance = point.getDistanceTo(mMassCenterX, mMassCenterY);
        if(pointDistance > mRadius){
            mRadius = pointDistance;
        }
    }

    /**
     * Moves the point by a specified amount
     * @param point the point to be moved
     * @param deltaX the change in the X direction
     * @param deltaY the change in the Y direction
     */
    public void movePoint(LatticePoint point, float deltaX, float deltaY) {
        //recompute the center of mass before we change the point;
        removePointFromCenterOfMass(point);
        point.move(deltaX, deltaY);
        addPointToMetadata(point);
    }

    public void setPoint(LatticePoint point, float newX, float newY) {
        removePointFromCenterOfMass(point);
        point.setValues(newX, newY);
        addPointToMetadata(point);
    }

    private void removePointFromCenterOfMass(LatticePoint point){
        mRunningTotalPointsX -= point.getX();
        mRunningTotalPointsY -= point.getY();
    }

    public void stageNewCoordinates(LatticePoint point, float newX, float newY) {
        point.stage(newX, newY);
    }

    /**
     * returns all of the points in the current Lattice
     * @return a List of points in the current lattice
     */
    public List<LatticePoint> getPoints() {
        return mLatticePoints;
    }

    /**
     * @return the number of points in the lattice
     */
    public int size() {
        return mLatticePoints.size();
    }

    /**
     * Rigidity is how resitant to change the lattice is.
     * A completely rigid lattice (1.0f) will not deform;
     * A completely loose lattice (0.0f) will deform completely
     * @return the current rigidity of the lattice
     */
    public float getRigidity() {
        return mRigidity;
    }

    /**
     * Rigidity is how resitant to change the lattice is.
     * A completely rigid lattice (1.0f) will not deform;
     * A completely loose lattice (0.0f) will deform completely
     * @param rigidity the new rigidity. A value between 0.0f and 1.0f;
     */
    public void setRigidity(float rigidity) {
        if(rigidity < 0f || rigidity > 1f){
            throw new RuntimeException("Rigidity must be between 0.0f and 1.0f");
        }
        mRigidity = rigidity;
    }

    /**
     * Elasticity is how fast the lattice will return to its original shape
     * A completely elastic lattice (1.0) will return to its shape in the next tick.
     * A completely inelastic lattice (0.0) will never return to its original shape.
     * @return the current elasticity of the lattice
     */
    public float getElasticity() {
        return mElasticity;
    }

    /**
     * Elasticity is how fast the lattice will return to its original shape
     * A completely elastic lattice (1.0) will return to its shape in the next tick.
     * A completely inelastic lattice (0.0) will never return to its original shape.
     * @param elasticity the new elasticity. A value between 0.0f and 1.0f;
     */
    public void setElasticity(float elasticity) {
        if(elasticity < 0f || elasticity > 1f){
            throw new RuntimeException("Elasticity must be between 0.0f and 1.0f");
        }
        mElasticity = elasticity;
    }


    public float getCenterOfMassY() {
        return mMassCenterY;
    }

    public float getCenterOfMassX() {
        return mMassCenterX;
    }

    public float getCenterAdjustedX(int point) {
        return mLatticePoints.get(point).getX() - mMassCenterX;
    }

    public float getCenterAdjustedX(LatticePoint point) {
        return point.getX() - mMassCenterX;
    }

    public float getCenterAdjustedY(int point) {
        return mLatticePoints.get(point).getY() - mMassCenterY;
    }

    public float getCenterAdjustedY(LatticePoint point) {
        return point.getY() - mMassCenterY;
    }

    public float getRadius() {
        return mRadius;
    }

    public LatticePoint getCenterMostPoint() {
        if(mCenterMostPoint == null){
            mCenterMostPoint = getPointNear(mMassCenterX, mMassCenterY);
        }
        return mCenterMostPoint;
    }

    /**
     * Simple linear check to find the closest point to the point provided
     * Iterats over all the points to find the closest point
     * TODO: Build a quadtree
     * @param x
     * @param y
     */
    public LatticePoint getPointNear(float x, float y) {
        LatticePoint closestPoint = mLatticePoints.get(0);
        double closestDistance = PhysUtils.distance(x, y, closestPoint.getX(), closestPoint.getY());

        for (LatticePoint point : mLatticePoints) {
            double distance = PhysUtils.distance(x, y, point.getX(), point.getY());
            if(Math.abs(distance) < Math.abs(closestDistance)){
                closestPoint = point;
                closestDistance = distance;
            }
        }
        return closestPoint;
    }

    public LatticePoint getAnyPoint() {
        return mLatticePoints.get(0);
    }


    public LatticePoint getPoint(int index) {
        return mLatticePoints.get(index);
    }

    public LatticeIterator getIterator(){
        return new LatticeIterator(this);
    }

    public void applyStageToPoints() {
        for(int point = 0; point < size(); point++){
            LatticePoint latticePoint = getPoint(point);
            removePointFromCenterOfMass(latticePoint);
            latticePoint.applyStage();
            addPointToMetadata(latticePoint);
        }
    }

    /**
     * One time set of if a lattice is a mesh. A mesh lattice asserts that the points are ordered in
     * row major order and that the lattice doesn't re-order its points.
     * @param isMesh
     */
    public void setMesh(boolean isMesh) {
        if(mIsMesh == null){
            mIsMesh = new Boolean(isMesh);
        }
    }

    public boolean isMesh() {
        if(mIsMesh != null){
            return mIsMesh;
        } else {
            return false;
        }
    }

    public void addOutlinePoint(LatticePoint point) {
        mOutlinePoint.add(point);
    }

    public Path getOutlinePath() {
        mOutlinePath.reset();
        LatticePoint point = mOutlinePoint.get(0);
        mOutlinePath.moveTo(getCenterAdjustedX(point), getCenterAdjustedY(point));
        for(int outline = 1; outline < mOutlinePoint.size(); outline++){
            point = mOutlinePoint.get(outline);
            mOutlinePath.lineTo(getCenterAdjustedX(point), getCenterAdjustedY(point));
        }
        mOutlinePath.close();
        return mOutlinePath;
    }



}
