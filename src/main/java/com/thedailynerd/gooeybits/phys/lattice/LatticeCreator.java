package com.thedailynerd.gooeybits.phys.lattice;

import com.thedailynerd.gooeybits.Constants;

/**
 * Created by Nick on 10/16/2015.
 */
public class LatticeCreator {

    public static final Lattice createMeshLattice(int meshSize){
        float meshSizeF = meshSize;
        LatticePoint[][] latticePoints = new LatticePoint[meshSize][meshSize];
        Lattice resultLattice = new Lattice();

        for(int y = 0; y < meshSize; y++){
            for(int x = 0; x < meshSize; x++){
                LatticePoint point = new LatticePoint( x / meshSizeF, y / meshSizeF, 2);
                latticePoints[x][y] = point;
                if(x > 0){
                    LatticePoint conn = latticePoints[x-1][y];
                    conn.addConnection(point);
                    point.addConnection(conn);
                }
                if(y > 0){
                    LatticePoint conn = latticePoints[x][y-1];
                    conn.addConnection(point);
                    point.addConnection(conn);
                }

                resultLattice.addPoint(point);
                if(x == 0 || y == 0 || x == meshSize -1 || y == meshSize - 1){
                    resultLattice.addOutlinePoint(point);
                }
            }
        }

        for(int outline = 0; outline < meshSize; outline++){
            resultLattice.addOutlinePoint(latticePoints[outline][0]);
        }
        for(int outline = 0; outline < meshSize; outline++){
            resultLattice.addOutlinePoint(latticePoints[meshSize - 1][outline]);
        }
        for(int outline = meshSize - 1; outline >= 0; outline--){
            resultLattice.addOutlinePoint(latticePoints[outline][meshSize - 1]);
        }
        for(int outline = meshSize - 1; outline >= 0; outline--){
            resultLattice.addOutlinePoint(latticePoints[0][outline]);
        }

        resultLattice.setRigidity(Constants.SANE_RIGIDITY);
        resultLattice.setElasticity(Constants.SANE_ELASTICITY);
        resultLattice.setMesh(true);

        return resultLattice;
    }

}
