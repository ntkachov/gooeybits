package com.thedailynerd.gooeybits.phys.lattice.ops;

import com.thedailynerd.gooeybits.debug.DebugSettings;
import com.thedailynerd.gooeybits.debug.TestLattice;
import com.thedailynerd.gooeybits.phys.lattice.Lattice;
import com.thedailynerd.gooeybits.phys.lattice.LatticePoint;
import com.thedailynerd.gooeybits.phys.utils.HitUtils;
import com.thedailynerd.gooeybits.phys.utils.PathUtils;

import java.util.List;

/**
 * Created by Nick on 7/25/15.
 */
public class LatticeOpBuilder {

    private LatticeOp mLatticeOp;

    LatticeOpBuilder(LatticeOp op){
        mLatticeOp = op;
    }

    public static LatticeOp newCenterOfMassOp(float x, float y) {
        return new LatticeOp().getBuilder().onCenterOfMass().move(x,y).build();
    }

    public LatticeOpBuilder getPointNear(final float x, final float y){
        mLatticeOp.mLatticeResolver = new LatticeOp.LatticeResolver() {
            @Override
            public LatticePoint[] getPoint(Lattice lattice) {
                return new LatticePoint[]{
                    lattice.getPointNear(x, y)
                };
            }
        };
        return this;
    }

    public LatticeOpBuilder onAllPoints(){
        mLatticeOp.mLatticeResolver = new LatticeOp.LatticeResolver() {
            @Override
            public LatticePoint[] getPoint(Lattice lattice) {
                LatticePoint[] latticePoints = new LatticePoint[lattice.size()];
                lattice.getPoints().toArray(latticePoints);
                return latticePoints;
            }
        };
        return this;
    }

    public LatticeOpBuilder pushRadius(final float x, final float y){
        mLatticeOp.mLatticeResolver = new LatticeOp.LatticeResolver() {
            @Override
            public LatticePoint[] getPoint(Lattice lattice) {
                List<LatticePoint> points = HitUtils.getPointsInRadius(lattice, x, y, DebugSettings.DEFAULT_RADIUS);
                LatticePoint[] latticePoints = new LatticePoint[points.size()];
                points.toArray(latticePoints);
                return latticePoints;
            }
        };
        return this;
    }

    public LatticeOp build() {
        return mLatticeOp;
    }

    public LatticeOpBuilder move(final float x, final float y) {
        mLatticeOp.mPointResolver = new LatticeOp.PointResolver() {
            @Override
            public void doAction(Lattice lattice, LatticePoint[] points) {
                for(LatticePoint point : points){
                    lattice.movePoint(point, x, y);
                }
            }
        };
        return this;
    }

    public LatticeOpBuilder set(final float x, final float y){
        mLatticeOp.mPointResolver = new LatticeOp.PointResolver() {
            @Override
            public void doAction(Lattice lattice, LatticePoint[] points) {
                for(LatticePoint point : points){
                    lattice.setPoint(point, x, y);
                }
            }
        };
        return this;
    }

    public LatticeOpBuilder onCenterOfMass() {
        mLatticeOp.mLatticeResolver = new LatticeOp.LatticeResolver(){

            @Override
            public LatticePoint[] getPoint(Lattice lattice) {
                return new LatticePoint[] {
                        lattice.getPointNear(lattice.getCenterOfMassX(), lattice.getCenterOfMassY())
                };
            }
        };
        return this;
    }

    public LatticeOpBuilder onAnyPoint(){
        mLatticeOp.mLatticeResolver = new LatticeOp.LatticeResolver(){

            @Override
            public LatticePoint[] getPoint(Lattice lattice) {
                return new LatticePoint[] {
                        lattice.getAnyPoint()
                };
            }
        };
        return this;
    }

    public LatticeOpBuilder onTestPoint(){
        mLatticeOp.mLatticeResolver = new LatticeOp.LatticeResolver(){

            @Override
            public LatticePoint[] getPoint(Lattice lattice) {
                return new LatticePoint[] {
                        TestLattice.dragPoint
                };
            }
        };
        return this;
    }


    public LatticeOpBuilder multiply(final float i) {
        mLatticeOp.mPointResolver = new LatticeOp.PointResolver() {
            @Override
            public void doAction(Lattice lattice, LatticePoint[] points) {
                for(LatticePoint point : points){
                    lattice.movePoint(point, point.getX() * i, point.getY() * i);
                }
            }
        };
        return this;
    }

    public LatticeOpBuilder doNothing() {
        mLatticeOp.mPointResolver = new LatticeOp.PointResolver() {
            @Override
            public void doAction(Lattice lattice, LatticePoint[] points) {
                //do nothing
            }
        };
        mLatticeOp.mLatticeResolver = new LatticeOp.LatticeResolver() {
            @Override
            public LatticePoint[] getPoint(Lattice lattice) {
                return new LatticePoint[0];
            }
        };
        return this;
    }
}
