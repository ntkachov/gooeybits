package com.thedailynerd.gooeybits.phys.lattice;

/**
 * Helper class to create pairs of lattices with a unique hashcode for indexing connections
 */
public class LatticePair{

    LatticePoint mA ,mB;
    LatticePoint.LatticeConnection mABConnection, mBAConnection;
    /**
     * Creates a POJO wrapper around 2 points with some helper methods.
     * @param pointA a lattice point
     * @param pointB another lattice point (note: can be the same as A)
     */
    public LatticePair(LatticePoint pointA, LatticePoint pointB){
        mA = pointA; mB = pointB;
        mABConnection = mA.findConnectionTo(mB);
        mBAConnection = mB.findConnectionTo(mA);
    }

    /**
     * Returns the hashcode of mA combined with the hashcode for mB
     * This hashcode NEEDS to be communicative. Meaning hashcode(mA,mB) == hashcode(mB,mA)
     * @return
     */
    @Override
    public int hashCode() {
        return mA.hashCode() + mB.hashCode();
    }

    /**
     * Convenience method to get the distance between mA and mB.
     * Simply calls PhysUtils.distance();
     * @return
     */
    public float getDistance() {
        return mA.getDistance(mB);
    }

    public Double getAngle() {
        return mA.getAngle(mB);
    }

    public LatticePoint getA() {
        return mA;
    }

    public LatticePoint getB() {
        return mB;
    }
}