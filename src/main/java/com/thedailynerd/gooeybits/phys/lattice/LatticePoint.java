package com.thedailynerd.gooeybits.phys.lattice;

import com.thedailynerd.gooeybits.phys.utils.PhysUtils;

import java.util.ArrayList;

/**
 * Created by Nick on 7/18/15.
 */
public class LatticePoint{

    public static final double RELATIVE_X = 0, RELATIVE_Y = 0;
    public static final double RELATIVE_DISTANCE = 1;
    public static final float DEFAULT_WEIGHT = 1;
    public static final double PI2 = Math.PI * 2;

    private float mX, mY, mStageX, mStageY;
    private float mPrevX, mPrevY;
    private int mStageCount;
    private final ArrayList<LatticeConnection> mConnections;
    private float mWeight = DEFAULT_WEIGHT;
    private boolean mLocked;

    /**
     * A lattice point contains a vertex in the lattice mesh
     * @param x the x coordinate of the lattice relative to the other points.
     *          Lattice coordinates are in their own coordinate system.
     * @param y the y coordinate of the lattice relative to the other points.
     *          Lattice coordinates are in their own coordinate system.
     * @param connections the number of connections to other lattice points
     *                    that this lattice will have. Used to initialize
     *                    the connection list size.
     */
    public LatticePoint(float x, float y, int connections){
        mX = x; mY = y;
        mPrevX = x; mPrevY = y;
        mConnections = new ArrayList<>(connections);
    }

    /**
     * Adds a connection to the LatticePoint. Whatever the values are at this point will be the
     * values that the lattice will try to preserve. Note: this connection is a 1 way connection.
     * If you connect this point to another point, it will move the other point but the other point
     * will not move this point unless it is also connected to this point.
     * @param point the point that will be added to the lattice.
     */
    public void addConnection(LatticePoint point){
        mConnections.add(new LatticeConnection(point));
    }

    public void addConnection(LatticePoint... points){
        for(LatticePoint point : points){
            addConnection(point);
        }
    }

    public float getDistance(LatticePoint point) {
        return getDistanceTo(point.mX, point.mY);
    }

    public float getDistanceTo(float x, float y) {
        return PhysUtils.distance(x, y, mX, mY);
    }
    public double getAngle(LatticePoint point) {
        double angle =  Math.atan2(point.mY - mY, point.mX - mX);
        if(angle <= 0){
            angle = PI2 + angle;
        }
        return angle;
    }

    void move(float deltaX, float deltaY) {
        if(!mLocked) {
            mX += deltaX;
            mY += deltaY;
        }
    }

    void setValues(float newX, float newY) {
        if(!mLocked) {
            mX = newX;
            mY = newY;
        }
    }

    void stage(float newX, float newY) {
        mStageX += newX;
        mStageY += newY;
        mStageCount++;
    }

    void applyStage(){
        if(mStageCount == 0) {
            return;
        }
        swapStage();
    }

    public float getWeight() {
        return mWeight;
    }

    public void setWeight(float weight){
        mWeight = weight;
    }

    /**
     * Scans all of the connections to the point to find the connection to the other point
     * @param otherPoint the other point that this point is connected to
     * @return the connection between this point and otherPoint. Or null if the two points are not
     * connected
     */
    public LatticeConnection findConnectionTo(LatticePoint otherPoint) {
        for(LatticeConnection connection : mConnections){
            if(connection.getConnectedPoint() == otherPoint){
                return connection;
            }
        }
        return null;
    }

    public void asVector(float[] vector) {
        vector[0] = getX();
        vector[1] = getY();
    }

    public void lock() {
        mLocked = true;
    }

    public float getStageX() {
        return mStageX / mStageCount;
    }

    public float getStageY(){
        return mStageY / mStageCount;
    }

    public float getVelocityX() {
        return mX - mPrevX;
    }

    public float getVelocityY() {
        return mY - mPrevY;
    }

    private void swapStage(){
        mPrevX = mX;
        mPrevY = mY;
        setValues(getStageX(), getStageY());
        mStageCount = 0;
        mStageX = 0;
        mStageY = 0;
    }



    public class LatticeConnection {
        final LatticePoint mConnectedTo;
        float mDesiredDistance;
        double mDesiredAngle;

        private LatticeConnection(LatticePoint connectedTo){
            mConnectedTo = connectedTo;
            mDesiredDistance = getCurrentDistance();
            mDesiredAngle = getCurrentAngle();
        }

        public void setDesiredDistance(float distance){
            mDesiredDistance = distance;
        }

        public double getY() {
            return mConnectedTo.getY();
        }

        public double getX() {
            return mConnectedTo.getX();
        }

        public float getDesiredDistance() {
            return mDesiredDistance;
        }

        public double getDesiredAngle() {
            return mDesiredAngle;
        }

        public LatticePoint getConnectedPoint() {
            return mConnectedTo;
        }

        public float getCurrentDistance() {
            return LatticePoint.this.getDistance(mConnectedTo);
        }

        public LatticePoint getDominant() {
            return LatticePoint.this;
        }

        public double getCurrentAngle() {
            return getDominant().getAngle(mConnectedTo);
        }
    }

    public ArrayList<LatticeConnection> getConnections() {
        return mConnections;
    }

    public float getY() {
        return mY;
    }

    public float getX() {
        return mX;
    }

    @Override
    public String toString() {
        return "[" + mX + " , " + mY + "]";
    }
}