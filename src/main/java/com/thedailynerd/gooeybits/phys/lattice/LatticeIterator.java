package com.thedailynerd.gooeybits.phys.lattice;

import android.util.Log;

import java.util.Iterator;

/**
 * Created by Nick on 8/9/2015.
 */
public class LatticeIterator implements Iterator<LatticePoint.LatticeConnection> {

    private final Lattice mLattice;
    private int pointIter, connectionIter;

    public LatticeIterator(Lattice lattice){
        mLattice = lattice;
        reset();
    }

    @Override
    public boolean hasNext() {
        if(connectionIter + 1 < mLattice.getPoint(pointIter).getConnections().size()){
            return true;
        } else {
            int pointMax = mLattice.size();
            return pointIter + 1 < pointMax;
        }
    }

    @Override
    public LatticePoint.LatticeConnection next() {
        LatticePoint point = mLattice.getPoint(pointIter);
        connectionIter++;
        if(connectionIter >= point.getConnections().size()){
            pointIter++;
            connectionIter = 0;
        }
        return mLattice.getPoint(pointIter).getConnections().get(connectionIter);
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException("Iterator cannot modify the lattice");
    }

    public void reset(){
        pointIter = 0;
        connectionIter = -1;
    }
}
