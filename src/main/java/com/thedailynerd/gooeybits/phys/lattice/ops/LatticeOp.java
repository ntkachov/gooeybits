package com.thedailynerd.gooeybits.phys.lattice.ops;

import com.thedailynerd.gooeybits.phys.lattice.Lattice;
import com.thedailynerd.gooeybits.phys.lattice.LatticePoint;

/**
 * Created by Nick on 7/25/15.
 */
public class LatticeOp {

    LatticeResolver mLatticeResolver;
    PointResolver mPointResolver;

    public LatticeOpBuilder getBuilder(){
        return new LatticeOpBuilder(this);
    }

    public void operate(Lattice lattice){
        LatticePoint[] point = mLatticeResolver.getPoint(lattice);
        mPointResolver.doAction(lattice, point);
    }

    interface LatticeResolver {
        LatticePoint[] getPoint(Lattice lattice);
    }

    interface PointResolver {
        void doAction(Lattice lattice, LatticePoint[] points);
    }


}
