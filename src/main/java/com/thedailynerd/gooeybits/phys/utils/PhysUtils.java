package com.thedailynerd.gooeybits.phys.utils;

import android.graphics.Matrix;
import android.util.Log;

import com.thedailynerd.gooeybits.ThreadUtils;
import com.thedailynerd.gooeybits.debug.DebugSettings;
import com.thedailynerd.gooeybits.phys.lattice.LatticePoint;


/**
 * Created by Nick on 7/18/15.
 */
public class PhysUtils {

    public static final double SIGNIFICANT_DIFFERENCE = 0.0000001;
    public static final double PI2 = Math.PI * 2;

    public static float distance (float x1, float y1, float x2, float y2){
        double xsq = Math.pow((x2 - x1), 2);
        double ysq = Math.pow((y2 - y1), 2);
        return (float) Math.sqrt(xsq + ysq);
    }

    public static double getSignedShortestDistanceAngle(double desiredAngle, double currentAngle){
        double diff = ( currentAngle - desiredAngle + Math.PI ) % PI2 - Math.PI;
        return diff < -Math.PI ? diff + PI2 : diff;
    }

    public static double clamp(double val, double min, double max){
        return Math.max(min, Math.min(max, val));
    }

    public static double getAngleBetweenTwoVectors(float vec1X, float vec1Y, float vec2X, float vec2Y){
        float denom = distance(0, 0, vec1X, vec1Y) * distance(0, 0, vec2X, vec2Y);
        if(denom == 0) { return 0; }
        float dot = (vec1X * vec2X) + (vec1Y * vec2Y);
        if(dot == 0) { return 0; }
        return Math.acos(dot / denom);
    }

    public static float unitize(float value, float x, float y){
        return value / distance(0,0,x,y);
    }

    public static double distanceToLineSegment(float pointX, float pointY, float lineX1, float lineY1, float lineX2, float lineY2){
        double nominator = ((pointX - lineX1)*(lineX2-lineX1))+((pointY - lineY1)*(lineY2-lineY1));
        double denominator = distance(lineX1, lineY1, lineX2, lineY2);
        return nominator / denominator;
    }

}
