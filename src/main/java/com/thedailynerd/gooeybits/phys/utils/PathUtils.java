package com.thedailynerd.gooeybits.phys.utils;

import android.graphics.Path;

import com.thedailynerd.gooeybits.phys.lattice.Lattice;
import com.thedailynerd.gooeybits.phys.lattice.LatticeIterator;
import com.thedailynerd.gooeybits.phys.lattice.LatticePoint;

/**
 * Created by Nick on 8/24/2015.
 */
public class PathUtils {

    public static Path toSimplePath(Lattice lattice){
        Path path = new Path();

        LatticePoint point = lattice.getAnyPoint();
        path.moveTo(point.getX(), point.getY());
        LatticePoint prevPoint = point;
        LatticePoint nextPoint = point.getConnections().get(0).getConnectedPoint();
        while(nextPoint != point){
            if(nextPoint.getConnections().size() > 2){
                throw new RuntimeException("More than 1 connection per point, use convex hull path");
            }

            path.lineTo(nextPoint.getX(), nextPoint.getY());

            LatticePoint nextCandidate = nextPoint.getConnections().get(0).getConnectedPoint();
            if(nextCandidate == prevPoint){
                prevPoint = nextPoint;
                nextPoint = prevPoint.getConnections().get(1).getConnectedPoint();
            } else {
                prevPoint = nextPoint;
                nextPoint = nextCandidate;
            }
        }

        path.close();
        return path;
    }

    public void toConvexHullPath(Lattice lattice){

    }
}
