package com.thedailynerd.gooeybits.phys.utils.simulation;

import android.util.Log;

import com.thedailynerd.gooeybits.ViewProperties;
import com.thedailynerd.gooeybits.ViewPropertiesApplicator;
import com.thedailynerd.gooeybits.debug.DebugFrameCalculator;
import com.thedailynerd.gooeybits.debug.TestLattice;
import com.thedailynerd.gooeybits.phys.lattice.Lattice;
import com.thedailynerd.gooeybits.phys.lattice.LatticeIterator;
import com.thedailynerd.gooeybits.phys.lattice.LatticePoint;
import com.thedailynerd.gooeybits.phys.lattice.ops.LatticeOp;
import com.thedailynerd.gooeybits.phys.lattice.ops.LatticeOpBuilder;
import com.thedailynerd.gooeybits.phys.lattice.ops.LatticeOperator;
import com.thedailynerd.gooeybits.phys.utils.PhysUtils;


/**
 * Created by Nick on 7/5/15.
 */
public class Goop {

    private Lattice mLattice;
    private LatticeOperator mLatticeOps;
    private boolean mDrawDesired;
    private boolean mSettled;
    // Cache the lattice iterators so we don't need to create a new one every frame.
    private LatticeIterator mConnectionIter, mEnsureRigidity_LatticeIter, mVelocityIter;
    private FrameCalculator frame;
    private ConnectionSimulator connectionSimulator;
    private VelocitySimulator velocitySimulator;
    private ViewPropertiesApplicator mViewPropertyApplicator;

    public Goop(Lattice lattice){
        mLattice = lattice;
        mLatticeOps = new LatticeOperator(lattice);
        mConnectionIter = mLattice.getIterator();
        mEnsureRigidity_LatticeIter = mLattice.getIterator();
        mVelocityIter = mLattice.getIterator();
        frame = new FrameCalculator();
        connectionSimulator = new ConnectionSimulator(frame);
        velocitySimulator = new VelocitySimulator(frame);
        mViewPropertyApplicator = new ViewPropertiesApplicator(this);
    }

    public void compute(ViewProperties viewProperties) {
        mViewPropertyApplicator.apply(viewProperties);
    }

    public void doAction(LatticeOp interaction){
        mLatticeOps.resolve(interaction);
        ensureRigidity();
        frame.resetFrame();
    }

    public boolean isSettled(){
        return mSettled;
    }

    public void runFrame() {
        mSettled = false;
        frame.advanceFrame();
        connectionSimulator.onFrame(mLattice, mConnectionIter);
        velocitySimulator.onFrame(mLattice);
        applyStageToPoints();
    }

    private void ensureRigidity(){
        mEnsureRigidity_LatticeIter.reset();
        while(mEnsureRigidity_LatticeIter.hasNext()){
            LatticePoint.LatticeConnection connection = mEnsureRigidity_LatticeIter.next();
            correctConnectionForRigidity(connection, mLattice.getRigidity());
        }
    }

    private void correctConnectionForRigidity(LatticePoint.LatticeConnection connection, float rigidity){
        float newDistance = calculateDistanceForRigidity(rigidity, connection);
        double newAngle = calculateRotationForRigidity(rigidity, connection);
        connectionSimulator.correctConnection(mLattice, connection, newAngle, newDistance);
    }

    public float calculateDistanceForRigidity(float rigidity, LatticePoint.LatticeConnection connection) {
        double minDistance = connection.getDesiredDistance() * rigidity;
        double maxDistance = connection.getDesiredDistance() + (connection.getDesiredDistance() * (1 - rigidity));
        double currentDistance = connection.getCurrentDistance();
        return (float) PhysUtils.clamp(currentDistance, minDistance, maxDistance);
    }


    public double calculateRotationForRigidity(float rigidity, LatticePoint.LatticeConnection connection){
        double angleSlack = ((Math.PI / 2) * (1 - rigidity));
        double angle = PhysUtils.getSignedShortestDistanceAngle(connection.getDesiredAngle(), connection.getCurrentAngle());
        if(Math.abs(angle) > angleSlack ){
            angleSlack *= Math.signum(angle);
            return connection.getDesiredAngle() + angleSlack;
        } else {
            return connection.getCurrentAngle();
        }
    }

    private void applyStageToPoints(){
        mLattice.applyStageToPoints();
    }

    public Lattice getLattice() {
        return mLattice;
    }
}
