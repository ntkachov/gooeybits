package com.thedailynerd.gooeybits.phys.utils.simulation;

/**
 * Created by Nick on 9/26/2015.
 */
public class FrameCalculator {

    private static final long RESET = -1;
    private static final long SIMULATION_FRAME_TIME = 16; //Simualtion runs at 62.5fps
    private static final double SIMULATION_SPEED = 1;

    private long mTimeStep;
    private double mCurrentFramestep;

    public void resetFrame(){
        mTimeStep = RESET;
        mCurrentFramestep = RESET;
    }

    public void advanceFrame(){
        if(mTimeStep != RESET) {
            mCurrentFramestep = System.currentTimeMillis() - mTimeStep;
        }
        mTimeStep = System.currentTimeMillis();
    }

    public double getFrameStep(){
        if(mCurrentFramestep == RESET){
            //If our frame was reset, we advance forward 1 frame.
            return 1 * SIMULATION_SPEED;
        }
        //Advance forward by however many fractions of a frame have passed since the last frame
        return (mCurrentFramestep / (double)SIMULATION_FRAME_TIME) * SIMULATION_SPEED;
    }

}
