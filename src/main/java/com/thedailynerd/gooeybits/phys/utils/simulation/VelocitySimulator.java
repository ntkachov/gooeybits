package com.thedailynerd.gooeybits.phys.utils.simulation;

import android.util.Log;

import com.thedailynerd.gooeybits.phys.lattice.Lattice;
import com.thedailynerd.gooeybits.phys.lattice.LatticeIterator;
import com.thedailynerd.gooeybits.phys.lattice.LatticePoint;
import com.thedailynerd.gooeybits.phys.utils.PhysUtils;

import java.util.List;

/**
 * Created by Nick on 9/26/2015.
 */
class VelocitySimulator extends Simulator {

    public VelocitySimulator(FrameCalculator frameCalculator){
        super(frameCalculator);
    }

    public void onFrame(Lattice lattice){
        List<LatticePoint> points = lattice.getPoints();
        for(int iter = 0; iter < points.size(); iter++){
            applyVelocityToStage(lattice, points.get(iter), lattice.getRigidity());
        }
    }

    private void applyVelocityToStage(Lattice lattice, LatticePoint point, float riditidy) {
        float velocityResistance = 2 - riditidy;
        float velocityX = (float) getVelocity(point.getVelocityX(), velocityResistance) + point.getX();
        float velocityY = (float) getVelocity(point.getVelocityY(), velocityResistance) + point.getY();
        lattice.stageNewCoordinates(point, velocityX, velocityY);
    }

    private double getVelocity(float velocity, float velocityResistance){
        if(Math.abs(velocity) < PhysUtils.SIGNIFICANT_DIFFERENCE){
            return 0;
        }
        return velocity * mFrameCalculator.getFrameStep() * velocityResistance;
    }



}
