package com.thedailynerd.gooeybits.phys.utils;

import com.thedailynerd.gooeybits.phys.lattice.Lattice;
import com.thedailynerd.gooeybits.phys.lattice.LatticePoint;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nick on 9/17/2015.
 */
public class HitUtils {

    public static List<LatticePoint> getPointsInRadius(Lattice lattice, float x, float y, float radius){
        List<LatticePoint> points = new ArrayList<>();
        for(int iter = 0; iter < lattice.size(); iter++){
            LatticePoint point = lattice.getPoint(iter);
            if(PhysUtils.distance(point.getX(), point.getY(), x, y) < radius){
                points.add(point);
            }
        }
        return points;
    }

    /**
     * Computes the furthest point from the center that is closest to the line x,y
     * @param lattice
     * @param x
     * @param y
     * @return
     */
    public static LatticePoint getDragPointOnVector(Lattice lattice, float x, float y){
        x = PhysUtils.unitize(x, x ,y) * lattice.getRadius();
        y = PhysUtils.unitize(y, x, y) * lattice.getRadius();
        return lattice.getPointNear(x + lattice.getCenterOfMassX(), y + lattice.getCenterOfMassY());
    }
}
