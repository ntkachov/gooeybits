package com.thedailynerd.gooeybits.phys.utils.simulation;

import android.graphics.Matrix;

import com.thedailynerd.gooeybits.ThreadUtils;
import com.thedailynerd.gooeybits.phys.lattice.Lattice;
import com.thedailynerd.gooeybits.phys.lattice.LatticeIterator;
import com.thedailynerd.gooeybits.phys.lattice.LatticePoint;
import com.thedailynerd.gooeybits.phys.utils.PhysUtils;

/**
 * Created by Nick on 9/26/2015.
 */
class ConnectionSimulator extends Simulator {

    private float[] mVectorCache;

    public ConnectionSimulator(FrameCalculator frameCalculator) {
        super(frameCalculator);
        mVectorCache = new float[2];
    }

    public void onFrame(Lattice lattice, LatticeIterator iterator){
        iterator.reset();
        while(iterator.hasNext()){
            LatticePoint.LatticeConnection connection = iterator.next();
            correctConnectionForElasticity(lattice, connection, lattice.getElasticity());
        }
    }

    private void correctConnectionForElasticity(Lattice lattice, LatticePoint.LatticeConnection connection, float elasticity) {
        float distance = calculateDistanceForPoints(elasticity, connection);
        double rotation = calculateNewAngleForPoints(elasticity, connection);
        correctConnection(lattice, connection, rotation, distance);
    }

    public void correctConnection(Lattice lattice, LatticePoint.LatticeConnection connection, double rotation, float distance){
        LatticePoint dom = connection.getDominant(), sub = connection.getConnectedPoint();
        getCoordinatesForDistanceAndAngle(dom, sub, rotation,
                distance, mVectorCache);
        setPoint(lattice, sub, mVectorCache);
    }

    private void setPoint(Lattice lattice, LatticePoint point, float[] vector){
        lattice.stageNewCoordinates(point, vector[0], vector[1]);
    }

    public float calculateDistanceForPoints(float elasticity, LatticePoint.LatticeConnection connection){
        double distanceCurrent = connection.getCurrentDistance();
        double distanceDesired = connection.getDesiredDistance();

        double weightCoefficient = getWeightCoefficientForConnection(connection);
        double distanceDiff = distanceDesired - distanceCurrent;
        return (float) calculateValueForElasticity(elasticity, distanceDiff, distanceDesired, distanceCurrent, weightCoefficient);
    }


    public double calculateNewAngleForPoints(float elasticity, LatticePoint.LatticeConnection connection){
        double weightCoefficient = getWeightCoefficientForConnection(connection);
        double distanceDiff = PhysUtils.getSignedShortestDistanceAngle(connection.getDesiredAngle(), connection.getCurrentAngle());
        return calculateValueForElasticity(elasticity, distanceDiff, connection.getDesiredAngle(), connection.getCurrentAngle(), weightCoefficient);
    }

    protected  double calculateValueForElasticity(float elasticity, double distanceDiff, double valueDesired, double valueCurrent, double weightCoefficient){
        double unit = mFrameCalculator.getFrameStep() * elasticity;
        if(unit == 0){ return valueCurrent; }

        double weightedDiff = distanceDiff * weightCoefficient;
        double diff = unit * weightedDiff;
        double newDistance = valueCurrent + diff;

//      Make sure we don't get into an infinite loop based off small differences.
        if(Math.abs(diff) < PhysUtils.SIGNIFICANT_DIFFERENCE || (Math.abs(diff) > Math.abs(distanceDiff))){
            return valueDesired;
        } else {
            return newDistance;
        }
    }

    protected static double getWeightCoefficientForConnection(LatticePoint.LatticeConnection connection){
        float weightDom = connection.getDominant().getWeight();
        float weightSub = connection.getConnectedPoint().getWeight();
        return PhysUtils.clamp(weightSub / weightDom, 0, 1);
    }

    public static float[] getCoordinatesForDistanceAndAngle(LatticePoint dom, LatticePoint sub, double desiredAngle,  float desiredDistance, float[] vector){
        float dist = PhysUtils.distance(sub.getX(), sub.getY(), dom.getX(), dom.getY());
        double angle = dom.getAngle(sub);
        vector[0] = ((sub.getX() - dom.getX()) / dist); //Normal vector of the connection
        vector[1] = ((sub.getY() - dom.getY()) / dist);


        double radians = PhysUtils.getSignedShortestDistanceAngle(desiredAngle, angle);
        Matrix rotationMatrix = ThreadUtils.getThreadLocalMatrix();
        //Rotate around the dominant by the radians
        rotationMatrix.setRotate((float) Math.toDegrees(radians));
        rotationMatrix.postScale(desiredDistance, desiredDistance);
        rotationMatrix.postTranslate(dom.getX(), dom.getY());
        rotationMatrix.mapPoints(vector);

        return vector;
    }
}
