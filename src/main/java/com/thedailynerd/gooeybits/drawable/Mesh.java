package com.thedailynerd.gooeybits.drawable;

import com.thedailynerd.gooeybits.debug.DebugSettings;
import com.thedailynerd.gooeybits.phys.lattice.Lattice;

/**
 * Created by Nick on 10/16/2015.
 */
public class Mesh {

    public float[] mVertecies;

    public Mesh(int meshSize){
        mVertecies = new float[(meshSize * meshSize) * 2];
    }

    public void fromLattice(Lattice lattice, int width, int height){
        if(lattice.isMesh()){
            int vertsIter = 0;
            for(int point = 0; point < lattice.size(); point++){
                mVertecies[vertsIter++] = lattice.getCenterAdjustedX(point) * width;
                mVertecies[vertsIter++] = lattice.getCenterAdjustedY(point) * height;
            }
        }
    }

    public float[] getVerts() {
        return mVertecies;
    }
}
