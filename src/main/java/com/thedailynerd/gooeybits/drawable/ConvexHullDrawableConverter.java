package com.thedailynerd.gooeybits.drawable;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.Log;

import com.thedailynerd.gooeybits.R;
import com.thedailynerd.gooeybits.debug.TestLattice;
import com.thedailynerd.gooeybits.debug.WireFrame;
import com.thedailynerd.gooeybits.phys.lattice.Lattice;
import com.thedailynerd.gooeybits.phys.lattice.LatticePoint;
import com.thedailynerd.gooeybits.phys.utils.PhysUtils;

import junit.framework.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nick on 10/7/2015.
 */
public class ConvexHullDrawableConverter {

    private double mPointAngleThreshold = 0.0872665; // 5 degrees in Rads

    public static Lattice testresults;
    private static int width, height;

    public void test(Resources resources){
        Bitmap bitmap = BitmapFactory.decodeResource(resources, R.drawable.android_logo);
        WireFrame.bitmapForMesh = bitmap;
        List<LatticePoint> points = scanPixels(bitmap);
        Log.d("DEBUG", points.toString());
        testresults = new Lattice();
        testresults.addAll(points.get(0));
        TestLattice.testLattice = testresults;
        testresults.setElasticity(.5f);
        testresults.setRigidity(.5f);
        TestLattice.dragPoint = testresults.getPoint(0);
    }


    public void getLatticeFromDrawable(Drawable drawable){
        if(drawable.getIntrinsicWidth() <= 0 && drawable.getIntrinsicHeight() <= 0){
            throw new RuntimeException("Invalid drawable. intrinsicHeight and width are <= 0");
        }

        Bitmap hullBitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ALPHA_8);
        Canvas canvas = new Canvas(hullBitmap);
        drawable.draw(canvas);

        //from the Image, scan the pixels from each side. The first non-zero pixel in each scanline is our point;
        List<LatticePoint> points = scanPixels(hullBitmap);

    }

    private List<LatticePoint> scanPixels(Bitmap bitmap){
        width = bitmap.getWidth();
        height = bitmap.getHeight();
        List<LatticePoint> bitmapPoints = new ArrayList<>((bitmap.getWidth() + bitmap.getHeight()) * 2);

        Point candidate = null;
//        Scan each row from the left
        for(int y = 0; y < bitmap.getHeight(); y++){
            int x = scanPixelRow(bitmap, y, 0, bitmap.getWidth());
                candidate = addLatticePoint(bitmapPoints, candidate, x,y);
        }

        //Scan each column from the bottom
        for(int x = candidate.x; x < bitmap.getWidth(); x++){
            int y = scanPixelColumn(bitmap, x, bitmap.getHeight() - 1, 0);
                candidate= addLatticePoint(bitmapPoints, candidate, x,y);
        }

        //Scan each column from the right starting from the bottom
        for(int y = candidate.y; y >= 0; y--){
            int x = scanPixelRow(bitmap, y, bitmap.getWidth() - 1, 0);
                candidate = addLatticePoint(bitmapPoints, candidate, x,y);
        }

        int end = (int) bitmapPoints.get(0).getX();
        //Scan each column from the top starting from the right
        for(int x = candidate.x; x >= end; x--){
            int y = scanPixelColumn(bitmap, x, 0, bitmap.getHeight());
                candidate = addLatticePoint(bitmapPoints, candidate, x,y);
        }

        LatticePoint firstPoint = bitmapPoints.get(0), lastpoint = bitmapPoints.get(bitmapPoints.size() -1 );
        firstPoint.addConnection(lastpoint);
        lastpoint.addConnection(firstPoint);
        return bitmapPoints;
    }

    private int scanPixelRow(Bitmap bitmap, int row, int colStart, int colEnd){
        int direction = getScanDirection(colStart, colEnd);
        for(int scan = colStart;  isDone(direction, scan, colEnd); scan += direction){
            int pixelColor = bitmap.getPixel(scan, row);
            if(Color.alpha(pixelColor) > 0){
                return scan;
            }
        }
        return -1;
    }

    private int scanPixelColumn(Bitmap bitmap, int col, int rowStart,int rowEnd){
        int direction = getScanDirection(rowStart, rowEnd);

        for(int scan = rowStart; isDone(direction, scan, rowEnd); scan += direction){
            int pixelColor = bitmap.getPixel(col, scan);
            if(Color.alpha(pixelColor) > 0){
                return scan;
            }
        }
        return -1;
    }

    private Point addLatticePoint(List<LatticePoint> collection, Point candidate, int x, int y){
        if(x >= 0 && y >= 0){
            if(collection.size() > 0 && candidate != null) {
                LatticePoint lastAdded = collection.get(collection.size() - 1);
                if (shouldAddPoint(lastAdded, candidate, x, y)) {
                    LatticePoint newPoint = new LatticePoint(candidate.x , candidate.y, 2);
                    collection.add(newPoint);
                    newPoint.addConnection(lastAdded);
                    lastAdded.addConnection(newPoint);
                    return new Point(x, y);
                } else {
                    candidate.set(x,y);
                    return candidate;
                }
            } else if (collection.size() == 1) {
                return new Point(x, y);
            } else {
                LatticePoint newPoint = new LatticePoint(x ,y,2);
                collection.add(newPoint);
                return null;
            }
        }
        return candidate;
    }

    private boolean shouldAddPoint(LatticePoint lastAdded, Point testPoint, float x, float y){
        float vec1X = testPoint.x - lastAdded.getX();
        float vec1Y = testPoint.y - lastAdded.getY();
        float vec2X = lastAdded.getX() - x;
        float vec2Y = lastAdded.getY() - y;
        double angle = PhysUtils.getAngleBetweenTwoVectors(vec1X, vec1Y, vec2X, vec2Y);
        return (angle % (Math.PI))> mPointAngleThreshold;
    }

    private boolean isDone(int direction, int scan, int end){
        if(direction < 0){
            return scan >= end;
        } else {
            return scan < end;
        }
    }

    private int getScanDirection(int start,int end){
        return (int) Math.signum(end - start);
    }
}
