package com.thedailynerd.gooeybits.drawable;

import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;

import com.thedailynerd.gooeybits.debug.TestLattice;
import com.thedailynerd.gooeybits.phys.lattice.Lattice;


/**
 * Created by Nick on 8/15/2015.
 */
public class DrawableConverter {

    Canvas mCanvas;

    public DrawableConverter(){
        mCanvas = new Canvas();
    }

    public Lattice getLatticeFromDrawable (Drawable drawable){
        Rect bounds = drawable.getBounds();
        if(bounds.width() != 0 && bounds.height() != 0){

        }

        drawable.draw(mCanvas);

        return TestLattice.getTestLattice();
    }
}
