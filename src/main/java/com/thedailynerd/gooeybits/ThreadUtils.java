package com.thedailynerd.gooeybits;

import android.graphics.Matrix;

import com.thedailynerd.gooeybits.phys.lattice.Lattice;
import com.thedailynerd.gooeybits.phys.lattice.LatticeIterator;

/**
 * Created by Nick on 8/8/2015.
 */
public class ThreadUtils {

    private static ThreadLocal<Matrix> threadLocalMatrix = new ThreadLocal<Matrix>(){
        @Override
        protected Matrix initialValue() {
            return new Matrix();
        }
    };

    public static Matrix getThreadLocalMatrix(){
       return threadLocalMatrix.get();
    }

}
